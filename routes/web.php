<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/aboutus', function () {
    return view('aboutUs');
});

Route::get('/applicationForm', function () {
    return view('applicationForm');
});
Route::get('/loanApplication', function () {
    return view('loanApplication');
});
Route::get('/shortTermLoan', function () {
    return view('shortTermLoan');
});