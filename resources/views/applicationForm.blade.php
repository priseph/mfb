@extends("layouts.masters")
@include('partials.navbar')
  
@include('partials.slideRight')
@section("content")
    <div class="Loan drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Loans & Mortgages</p>
                    <img src="images/Loans-and-mortgages-II@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Loan Product</p>
                    <li><a href="shortTermLoan.html">Short Term Loan</a></li>
                    <li><a href="rentLoan.html">Rent Loan</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Mortgage Product</p>
                    <li><a href="outrightPurchaseMortgage.html">Outright Purchase Mortgage</a></li>
                    <li><a href="valueAddedLoan.html">VAL Mortgage Loans</a></li>
                    <li><a href="constructionFinace.html">Construction Finance</a></li>
                    <li><a href="contstructionMortgage.html">Construction Mortgage</a></li>
                    <li><a href="equityRealease.html">Equity Release</a></li>
                    <li><a href="mortgageRefinance.html">Mortgage Refinance</a></li>
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                    <br><br>
                    <li><a href="homeImprovement.html">Home Improvement</a></li>
                    <li><a href="disporaMortgage.html">Diaspora Mortgage</a></li>
                    <li><a href="generationalMortgage.html">Generational Mortgage</a></li>
                    <li><a href="microMortgage.html">Micro Mortgage</a></li>
                    <li><a href="landAcquisition.html">Land Acquisition</a></li>
                    <li><a href="homeOwnershipMortgageAccount.html">Home Ownership Mortgage Account</a></li>
                </div>
            </div>
        </div>
    </div>
    <div class="savings drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Savings & Investments</p>
                    <img src="images/Savings@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Savings</p>
                    <li><a href="homePlan.html">Home Plan</a></li>
                    <li><a href="childrenSavingsAccount.html">Children Savings Account</a></li>
                    <li><a href="targetSavingAccount.html">Target Savings Account</a></li>
                    <li><a href="endowmentSavingsAccount.html">Endowment Savings Account</a></li>
                    <li><a href="firsttrustPremiumAccount.html">FirstTrust Premium Account</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Investments</p>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
      <!-- <div class="about-us drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>About Us</p>
                    <img src="images/Savings@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Our Firm</p>
                    <li>Bank Brief</li>
                    <li>The Merger</li>
                    <li>Core Values</li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>The Team</p>
                    <li>Board of Directors</li>
                    <li>Management Team</li>
                    
                    
                </div>
                <div class="col-md-3">
                   <i class="material-icons take_away">
                    cancel
                    </i>
                    <p>Others</p>
                    <li>Code of Governance</li>
                    <li>Careers</li>
                </div>
            </div>
        </div>
    </div> -->
    
    <div class="media drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Media</p>
                    <img src="images/Media@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Blog</p>
                    <li><a href="new.html">News</a></li>
                    <li><a href="video.html">Videos</a></li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>Resources</p>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="document.html">Documents</a></li>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
    <div class="investor drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Investor's Relations</p>
                    <img src="images/Investors%20Relations@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Others</p>
                    <li><a href="investorUpdate.html">Investors updates & records</a></li>
                    <li><a href="financial.html">Financials</a></li>
                </div>
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
 </div>
    
    <div class="container cooperate-application">
        <div class="row">
            <div class="col-md-4 list_one" style="color:#f7f7f7; background-color:#EAAB26;">
                <a href="applicationForm.html"  style="color:#f7f7f7">CORPORATE APPLICATION</a>
            </div>
            <div class="col-md-4 list_two">
                <a href="individualApplication.html" >INDIVIDUAL APPLICATION</a>
            </div>
            <div class="col-md-4 list_three">
                <a href="loanApplication.html" >LOAN APPLICATION</a>
            </div>
        </div>  
    </div><br>
      
      <div class="container thetoggleTabs">
      <div class="toppy">
       <p>Corperate Account Form</p>
        <ul class= "nav nav-tabs" role="tablist">
           
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#home">Company's Details</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu1">Signatory's Details</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu2">Director's Details</a>
            </li>
          </ul>
          </div>

              <!-- Tab panes -->
              <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                 <form action="">
                  <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="password" class="float-label">Company's Name</label>
                  <errorp>
                     it is required
                  </errorp>
                </div>
                
                  <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Certificate of Incorporation Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                 <div id="p" class="form-group">
                  <input id="date" class="form-control" spellcheck=false name="date" type="date" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Date of Incorporation</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Type of Business</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Operating Business Address</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Cooperate Business Address/Registered Office (If different from above)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <input id="email" class="form-control" spellcheck=false name="email" type="email" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Email</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Website</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div class="row" style="margin:0;">
                    <div id="p" class="form-group col-md-6" style="padding-left:0;">
                      <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                      <span class="form-highlight"></span>
                      <span class="form-bar"></span>
                      <label for="text" class="float-label">Phone Number(1)</label>
                      <errorp>
                        it is required
                      </errorp>
                    </div>

                    <div id="p" class="form-group col-md-6" style="padding:0px;">
                      <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                      <span class="form-highlight"></span>
                      <span class="form-bar"></span>
                      <label for="text" class="float-label">Phone Number(2)</label>
                      <errorp>
                        it is required
                      </errorp>
                    </div>
                </div>
                
                <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Tax Identification Number (TIN)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Annual Turnover</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                </form>
                    <a data-toggle="tab" href="#menu1"><button>Next</button></a>
            </div>
                
                
                

            <div id="menu1" class="container tab-pane fade"><br>
              <form action="">

              <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-4" style="padding-left:0;">
                  <input id="name" class="form-control" spellcheck=false name="nmae" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Surname</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">First Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Middle Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Residential Address</label>
              <errorp>
                it is required
              </errorp>
            </div>

              <div id="p" class="form-group">
              <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Occupation</label>
              <errorp>
                it is required
              </errorp>
            </div>
             
             <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-4" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Phone Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding-left:0px;">
                  <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                      <option value=""></option>
                      <option value="Female">Female</option>
                      <option value="Male">Male</option>
                  </select>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Gender</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="date" class="form-control" spellcheck=false name="date" type="date" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="date" class="float-label">Date of Birth</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>


            <div id="p" class="form-group">
              <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Mother's Maiden Name</label>
              <errorp>
                it is required
              </errorp>
            </div>
            </form>
            <a data-toggle="tab" href="#home"><button>Previous</button></a>
            <a data-toggle="tab" href="#menu2"><button>Next</button></a>
            
            </div>
            <div id="menu2" class="container tab-pane fade"><br>
              <form action="">

              <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-4" style="padding-left:0;">
                  <input id="name" class="form-control" spellcheck=false name="nmae" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Surname</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">First Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Middle Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Residential Address</label>
              <errorp>
                it is required
              </errorp>
            </div>

              <div id="p" class="form-group">
              <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Occupation</label>
              <errorp>
                it is required
              </errorp>
            </div>
             
             <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-4" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Phone Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding-left:0px;">
                  <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                      <option value=""></option>
                      <option value="Female">Female</option>
                      <option value="Male">Male</option>
                  </select>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Gender</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="date" class="form-control" spellcheck=false name="date" type="date" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="date" class="float-label">Date of Birth</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>


            <div id="p" class="form-group">
              <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Mother's Maiden Name</label>
              <errorp>
                it is required
              </errorp>
            </div>
            </form>
            <a data-toggle="tab" href="#menu1"><button>Previous</button></a>
            <a  href="applicationForm.html"><button>Submit</button></a>
            
            </div>
            </div>
          </div>
        </div>   

<!--
    <div class="helpful-tool container">
        <h2>Helpful Tools</h2>
        <div class="row">
            <div class="first col-md-4">
                <div class="inn">
                    <h3>Mortgage Calculator</h3>
                    <p>Find out how much you could borrow or<br> gain in investments</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Check our calculator</p>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>Application Form</h3>
                    <p>Get started on getting a loan or making an<br> investment with us</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Fill our online form</p>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>Fill our online form</h3>
                    <p>Check out the frequently asked questions from our customers.</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Go to FAQs</p>
                </div>
            </div>
        </div>
    </div>
-->
    
    
@endsection