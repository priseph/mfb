@extends("layouts.masters")
@section("content")
    <div class="modal fade" id="myModaCalculator">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header">  
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">
                <div class="sec-calculator bg-blue-fade py-3 py-md-5">
                    <div class="container my-4" >
                        <div class="col-lg-12 col-md-10 text-left" >
                            <h2 class="mb-4 section-title cl-blue">Loan Calculator</h2>
                            <p class="">Whether you are interested in Home ownership loan, Equity release loan, NHF, mortgage refinancing, or short-term loan; nothing beats being prepared. Our loan calculator will help you analyse the numbers involved in making your dream happen at the best rates.</p>
                        </div>
                        <div class="row justify-content-left">
                            <div class="col-md-6">
                                <div class="card border-0 mt-3 drop-shadow drop-shadow-xs">
                                    <div class="card-body px-lg-5 py-lg-4">
                                        <div class="row">
                                            <div class="form-group dropdown col-12 d-flex">
                                                <div class="col-12 col-md-10 mx-md-auto">
                                                    <button class="btn btn-yellow btn-sm dropdown-toggle w-100 py-2" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  type="button" >
                                                        Loan Type
                                                        <i class="mr-2 fas fa-angle-down"></i>
                                                    </button>
                                                    <div class="dropdown-menu nav" id="nav-tab" role="tablist" aria-labelledby="dropdownMenuButton" style="right: 30px">
                                                        <a class="dropdown-item" id="calc-one-tab" data-toggle="tab" href="#loan-calc-one" role="tab" aria-controls="loan-calc-one" aria-selected="true">Outright Purchase</a>
                                                        <a class="dropdown-item" id="calc-two-tab" data-toggle="tab" href="#loan-calc-two" role="tab" aria-controls="loan-calc-one" aria-selected="false">Construction Finance</a>
                                                        <a class="dropdown-item" id="calc-three-tab" data-toggle="tab" href="#loan-calc-three" role="tab" aria-controls="loan-calc-three" aria-selected="false">Equity Release</a>
                                                        <a class="dropdown-item" id="calc-four-tab" data-toggle="tab" href="#loan-calc-four" role="tab" aria-controls="loan-calc-four" aria-selected="false">NHF Loan</a>
                                                        <a class="dropdown-item" id="calc-five-tab" data-toggle="tab" href="#loan-calc-five" role="tab" aria-controls="loan-calc-five" aria-selected="false">Mortgage Refinancing</a>
                                                        <a class="dropdown-item" id="calc-six-tab" data-toggle="tab" href="#loan-calc-six" role="tab" aria-controls="loan-calc-six" aria-selected="false">Short Term Loan</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane fade show active" id="loan-calc-one" role="tabpanel" aria-labelledby="calc-one-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>Outright Purchase:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="24%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="loan-calc-two" role="tabpanel" aria-labelledby="calc-two-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>Construction Finance:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="24%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>    
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="loan-calc-three" role="tabpanel" aria-labelledby="calc-three-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>Equity Release:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="25%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="loan-calc-four" role="tabpanel" aria-labelledby="calc-four-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>NHF Loan:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="6%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="loan-calc-five" role="tabpanel" aria-labelledby="calc-five-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>Mortgage Refinancing:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="24%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="loan-calc-six" role="tabpanel" aria-labelledby="calc-six-tab">
                                                    <div class="col-lg row">
                                                        <div class="col-md-6">
                                                            <p class="text-muted mt-2 mb-0"><small>Short Term Loan:</small></p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="loan_amount" class="form-control b-line" placeholder="&#8358; Loan Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg row">
                                                        <div class="form-group col-md-6">
                                                            <input type="text" id="interest" value="28%" class="form-control b-line" placeholder="% interest Rate" disabled>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <input type="number" id="term" class="form-control b-line" placeholder="(Yrs) Term" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <a href="javascript:void(0)" id="calculate" class="btn btn-yellow btn-sm mt-3 py-2 px-4" onclick="preventDefault()">Calculate</a>
                                                    <a href="javascript:void(0)" class="btn btn-fbn btn-sm mt-3 py-2 px-4" id="reset" onclick="preventDefault()">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
                            <div class="col-md-6">
                                <div class="card border-0 mt-3 drop-shadow drop-shadow-xs">
                                    <div class="card-body px-lg-5 py-lg-4">
                                        <div class="row">
                                            <div class="col-8">
                                                <p>Per Month, You Repay
                                                    <br>
                                                    <small class="font-weight-light">assuming your interest rate stays the same</small>
                                                </p>
                                            </div>
                                            <div class="text-right mr-0">
                                                <p>&#8358;<span id="monthly_amount">0</span></p>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-8">
                                                <p>Total you'll repay over full term
                                                    <br>
                                                    <small class="font-weight-light">Does not include <b>Upfront Charges</b> (Committement Fee, Service Charge & Management Fee)</small>
                                                </p>
                                            </div>
                                            <div class="text-right mr-0">
                                                <p>&#8358;<span id="total_amount">0</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
            
                        </div>
                    </div>
                </div>
            </div>
      
          </div>
        </div>
      </div>

   


<div class="modal_content animated slideInRight">
        <div class="head">
            <a href="https://ibank.trustbondmortgagebankplc.com/IntBanking" target="_blank"><i class="material-icons lock">lock</i>Internet Banking</a>
                <i class="fas fa-times"></i>
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <h4 class="panel-title">
                        Account
                        <i class="fas fa-angle-down"></i>
                    </h4>
                </a>
                
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                        <p><a href="applicationForm.html">Accounts</a></p>
                        <li><a href="applicationForm.html">Application Form</a></li>
                        <li><a href="loanApplication.html"> Loan Application</a></li>
                    
                </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><h4 class="panel-title">
                    Loan & Mortages<i class="fas fa-angle-down"></i>
                    </h4></a>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                        <p><a href="loanAndMortages.html">Loan & Mortages</a></p>
                        <li><a href="rentLoan.html">Rent Loan</a></li>
                        <li><a href="shortTermLoan.html">Short Term Loan</a></li>
    
                        <p>Mortage Product</p>
                        <li><a href="outrightPurchaseMortgage.html">Outright Purchase Mortgage</a></li>
                        <li><a href="valueAddedLoan.html">VAL Mortgage Loans</a></li>
                        <li><a href="constructionFinace.html">Construction Finance</a></li>
                        <li><a href="contstructionMortgage.html">Construction Mortgage</a></li>
                        <li><a href="equityRealease.html">Equity Release</a></li>
                        <li><a href="mortgageRefinance.html">Mortgage Refinance</a></li>
                        <li><a href="homeImprovement.html">Home Improvement</a></li>
                        <li><a href="disporaMortgage.html">Diaspora Mortgage</a></li>
                        <li><a href="generationalMortgage.html">Generational Mortgage</a></li>
                        <li><a href="microMortgage.html">Micro Mortgage</a></li>
                        <li><a href="landAcquisition.html">Land Acquisition</a></li>
                        <li><a href="homeOwnershipMortgageAccount.html">Home Ownership Mortgage Account</a></li>
                    
                </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
                        Savings & Investments <i class="fas fa-angle-down"></i>
                    </h4></a>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                        <p><a href="savingAndInvestment.html">Savings And Investments</a></p>
                        <li><a href="homePlan.html">Home Plan</a></li>
                        <li><a href="childrenSavingsAccount.html">Children Savings Account</a></li>
                        <li><a href="targetSavingAccount.html">Target Savings Account</a></li>
                        <li><a href="endowmentSavingsAccount.html">Endowment Savings Account</a></li>
                        <li><a href="firsttrustPremiumAccount.html">FirstTrust Premium Account</a></li>
                    
                </div>
                </div>
            </div>
    
            <div class="panel panel-default">
                    <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><h4 class="panel-title">
                            About Us <i class="fas fa-angle-down"></i>
                        </h4></a>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <li><a href="aboutUs.html">About Us</a></li>
                    </div>
                    </div>
                </div>
    
                    <div class="panel panel-default">
                            <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><h4 class="panel-title">
                                    Media<i class="fas fa-angle-down"></i>
                                </h4></a>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                                    <li><a href="media.html">Media</a></li>
                            </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><h4 class="panel-title">
                                    Investors Relations<i class="fas fa-angle-down"></i>
                                </h4></a>
                            </div>

                            <div id="collapse7" class="panel-collapse collapse">
                            <div class="panel-body">
                                <li><a href="investorUpdate.html">Investors updates & records</a></li>
                                <li><a href="financial.html">Financials</a></li>
                            </div>
                            </div>
                        </div>
    
            </div>
    
            <h2>Quick Links</h2>
            <div class="quick">
                <li><a href="applicationForm.html">Open an Account</a></li>
                <li><a href="https://ibank.trustbondmortgagebankplc.com/IntBanking" target="_blank">Internet Banking</a></li>
                <li><a href="#"  data-toggle="modal" data-target="#myModaCalculator">Loan Calculator</a></li>
                <li><a href="savingAndInvestment.html">Investmnt Plan</a></li>
                <li><a href="contact.html">Customer Service</a></li>
            </div>
    
    </div>

@include('partials.navbar')

<div class="right">
    <div class="expgroup">
        <a href="https://web.facebook.com/ft.mortgagebankplc.7 " target="_blank"><button class="icon gplus">
            <i class="fab fa-facebook-f"></i>
        </button></a>
        <a href="" target="_blank"><button class="icon linkedin">
            <i class="fab fa-linkedin-in"></i>
        </button></a>
        <a href="" target="_blank"><button class="icon tumblr">
            <i class="fab fa-instagram"></i>
        </button></a>
        <a href="" target="_blank"><button class="icon reddit">
            <i class="fab fa-twitter"></i>
        </button></a>
        <a href="https://www.youtube.com/channel/UCqDNe0RcoX7mB4UCX9077EQ/?guided_help_flow=5" target="_blank"><button class="icon pocket">
            <i class="fab fa-youtube"></i>
        </button></a>
      </div>
    <div class="share">
        <i class="fas fa-share-alt"></i>
    </div>
    
</div>
<!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-ea289706-feeb-42d3-bbe3-eae3be07fbd7"></div> -->

<div class="why container">
    <div class="mx-auto">
        <h1 class="wow tada">Why FirstTrust</h1>
        <p>We have made getting loans and mortgages, and <br>making savings easier for you</p>
        <div class="row">
            <div class="col-md-2">
                <p>I want to</p> 
            </div>
            <div class="col-md-7">
                <input type="text" placeholder="Build a House"> 
            </div>
            <div class="col-md-3">
                <button>Search <span></span><span></span><span></span><span></span></button>
            </div>
        </div>
    </div>
    <div class="row pro">
      <div class="cancel">
          <p class="product">Products</p>
          <div class="cancle"><i class="material-icons">
cancel
</i></div>
      </div>
      <div class="col-md-4">
          <div class="cont">
              <p class="first">Loans & Mortgages</p>
              <h5>Construction Finances</h5>
              <p class="next">
                Construction finance bridges the gap between completed or partially completed work and you being paid.
              </p>
              <i class="material-icons">keyboard_arrow_right</i>
              
          </div>
      </div>
      <div class="col-md-4">
        <div class="cont">
            <p class="first">Loans & Mortgages</p>
            <h5>Construction Mortgage</h5>
            <p class="next">
                Loan borrowed to finance the construction of home and only interest is paid during the construction<br> period.
            </p>
            <i class="material-icons">keyboard_arrow_right</i>
        </div>
    </div>
    <div class="col-md-4">
        <div class="cont">
            <p class="first">Loans & Mortgages</p>
            <h5>Land Acquisition</h5>
            <p class="next">
              Construction finance bridges the gap between completed or partially completed work and you being paid.
            </p>
            <i class="material-icons">keyboard_arrow_right</i>
        </div>
    </div>

        <p style="margin-top:20px; font-size:12px; ">Can't find what you are looking for? <a href="#">Talk to an advisor</a></p>
    </div>
</div>

<div class="getting-started container-fluid">
       <div class="mx-auto">
        <h5>HOW TO GET STARTED</h5>
        <h1 class="wow tada">Everything in 3 Simple Steps</h1>
        <!-- <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#home">Loans & Mortgages</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu1">Savings and Investment</a>
            </li>
           </ul> -->
    </div>
             <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                  <div class="row">
                      <div class="col-md-4">
                          <img src="{{ asset('images/Browse%20out%20products.svg') }}" alt="">
                          <h5>Browse Our Products</h5>
                          <p>Check out our product list. From loans & mortgage products to savings & investments to our internet banking</p>
                      </div>
                      <div class="col-md-4">
                         <img src="{{ asset('images/Online%20application.svg') }}" alt="">
                          <h5>Fill out online application</h5>
                          <p>With our integrated online application. You can apply for any selected product(s).</p>
                      </div>
                      <div class="col-md-4">
                         <img src="{{ asset('images/Smile%20and%20sit%20back.svg') }}" alt="">
                          <h5>Smile and sit back</h5>
                          <p>We'll get back to you with your offer</p>
                      </div>
                  </div>
                </div>
                <div id="menu1" class="container tab-pane fade"><br>
                  <div class="row">
                      <div class="col-md-4">
                        
                          <img src="{{ asset('images/Smile%20and%20sit%20back.svg') }}" alt="">
                          <h5>Browse Our Products</h5>
                          <p>Check out our product list. From loans & mortgage products to savings & investments to our internet banking</p>
                      </div>
                      <div class="col-md-4">
                        
                           <img src="{{ asset('images/Browse%20out%20products.svg') }}" alt="">
                          <h5>Fill out online application</h5>
                          <p>With our integrated online application. You can apply for any selected product(s).</p>
                      </div>
                      <div class="col-md-4">
                         <img src="{{ asset('images/Online%20application.svg') }}" alt="">
                          <h5>Smile and sit back</h5>
                          <p>We'll get back to you with your offer</p>
                      </div>
                </div>
              </div>
            </div>
    </div>
    
    <div class="everything container">
       <div class="mx-auto">
        <h5>EVERYTHING YOU NEED</h5>
        <h1 class="wow tada">Products that meet your needs</h1>
        <p>We have offers that are tailored to your desires financially<br> from loans to savings to mortgages to investments</p>
        
       </div>
       <div class="tab">
          <button class="tablinks Bounce To Left" onclick="openCity(event, 'Loan')" id="defaultOpen">Loans<br><span>Use your money for profit</span></button>
          <button class="tablinks next-one" onclick="openCity(event, 'Mortgages')">Mortgages<br><span>Use your money for profit</span></button>
          <button class="tablinks next-one" onclick="openCity(event, 'Savings')">Savings<br><span>Use your money for profit</span></button>
          <button class="tablinks next-one" onclick="openCity(event, 'Investment')">Investments<br><span>Use your money for profit</span></button>
          <button class="tablinks next-one" onclick="openCity(event, 'Banking')">Online Banking<br><span>Use your money for profit</span></button>
        </div>

        <div id="Loan" class="tabcontent animated slideInRight">
          <div class="image"></div>
          <p>At FirstTrust we have a range of options available if and when you need funds, allowing you to make plans for the future. We provide an easy application process.</p>
          <span><i class="material-icons">keyboard_arrow_right</i>LEARN MORE</li></span>
        </div>

        <div id="Mortgages" class="tabcontent animated slideInRight">
          <div class="image_two"></div>
          <p>At FirstTrust we have a range of options available if and when you need funds, allowing you to make plans for the future. We provide an easy application process.</p>
          <span><i class="material-icons">keyboard_arrow_right</i>LEARN MORE</li></span> 
        </div>

        <div id="Savings" class="tabcontent animated slideInRight">
          <div class="image_three"></div>
          <p>At FirstTrust we have a range of options available if and when you need funds, allowing you to make plans for the future. We provide an easy application process.</p>
          <span><i class="material-icons">keyboard_arrow_right</i>LEARN MORE</li></span>
        </div>
        <div id="Investment" class="tabcontent animated slideInRight">
          <div class="image_four"></div>
          <p>At FirstTrust we have a range of options available if and when you need funds, allowing you to make plans for the future. We provide an easy application process.</p>
          <span><i class="material-icons">keyboard_arrow_right</i>LEARN MORE</li></span>
        </div>
        <div id="Banking" class="tabcontent animated slideInRight">
          <div class="image_five"></div>
          <p>At FirstTrust we have a range of options available if and when you need funds, allowing you to make plans for the future. We provide an easy application process.</p>
          <span><i class="material-icons">keyboard_arrow_right</i>LEARN MORE</li></span>
        </div>
        
    </div>
    <div class="who-we-are container-fluid">
        <div class="text  wow bounceInRight">
            <h1>We are FirstTrust</h1>
            <p>FirstTrust Mortgage Bank Plc is a mortgage bank resulting from the merger between First Mortgages Limited and TrustBond Mortgage Bank Plc with the intention of creating a strong financial institution that is capable of playing a headship role in the mortgage banking sector.<br></p>
            <a href="aboutUs.html"><button>Learn more About Us <span></span><span></span><span></span><span></span</button>
            </a>
            </div>
    </div>
    
    <div class="location container">
        <div class="row">
            <div class="col-md-6 description">
                <h5>LOCATIONS</h5>
                <h2>We strive beyond physical<br> state</h2>
                <p>124 Awolowo way, Ikoyi Lagos, <br>Nigeria</p>
                <li><i class="material-icons">keyboard_arrow_right</i>Give us a call</li>
                <!-- <li><i class="material-icons">keyboard_arrow_right</i>REACH ON US WHATSAPP</li> -->
                <li><i class="material-icons">keyboard_arrow_right</i>SEND US AN EMAIL</li>
            </div>
            <div class="col-md-6 image">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.6269197040874!2d3.4167925140173!3d6.441929995339409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b2b28b15883%3A0xe9972f91cd236351!2s124%20Awolowo%20Rd%2C%20Ikoyi%2C%20Lagos!5e0!3m2!1sen!2sng!4v1572653181050!5m2!1sen!2sng" width="585" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
    
    <div class="helpful-tool container">
        <h2>Helpful Tools</h2>
        <div class="row">
            <div class="first col-md-4">
                <div class="inn">
                    <h3>Mortgage Calculator</h3>
                    <p>Find out how much you could borrow or<br> gain in investments</p>
                    <p class="footer" data-toggle="modal" data-target="#myModaCalculator"><i class="material-icons">keyboard_arrow_right</i>Check our calculator</p>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>Application Form</h3>
                    <p>Get started on getting a loan or making an<br> investment with us</p>
                    <a href="applicationForm.html"><p class="footer"><i class="material-icons">keyboard_arrow_right</i>Fill our online form</p></a>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>FAQs</h3>
                    <p>Check out the frequently asked questions from our customers.</p>
                    <a href="contact.html"> <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Go to FAQs</p></a>
                </div>
            </div>
        </div>
    </div>

 @endsection

