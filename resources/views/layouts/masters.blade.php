<!DOCTYPE html>
<html lang="en">
<head>
  <title>FirstTrust</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="icon" href="{{ asset('images/Asset%202.svg') }}">
  
  <!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script type="text/javascript" src="{{ asset('javascript/jquery-2.2.4.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
  <script src="https://kit.fontawesome.com/799b643291.js" crossorigin="anonymous"></script>
  
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
  
 <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
 <link href="{{ asset('css/hover.css') }}" rel="stylesheet" media="all">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>


@yield('content')


@include('partials.footer')

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('javascript/jquery-2.2.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('javascript/script.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
     <script>
     new WOW().init();
     </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.carousel').carousel();
        });
    </script>
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5dfa30c843be710e1d22a748/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
    
</body>
</html>