
   @extends("layouts.masters")

@include('partials.modalCal')
@section("content")
    <div class="modal fade bd-example-modal-lg" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    arial-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Quick Application Form</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-3">
          <div class="md-form mb-5">
            <i class="fas fa-user prefix grey-text"></i>
            <label data-error="wrong" data-success="right" for="orangeForm-name"> Full Name</label>
            <input type="text" id="orangeForm-name" class="form-control validate">
          </div>
          <div class="md-form mb-5">
            <i class="fas fa-envelope prefix grey-text"></i>
              <label data-error="wrong" data-success="right" for="orangeForm-email">Email Address</label>
            <input type="email" id="orangeForm-email" class="form-control validate">
    
          </div>
    
          <div class="md-form mb-5">
            <i class="fas fa-envelope prefix grey-text"></i>
              <label data-error="wrong" data-success="right" for="orangeForm-number">BVN</label>
            <input type="number" id="orangeForm-number" class="form-control validate">
          </div>
    
          <div class="md-form mb-4">
            <i class="fas fa-lock prefix grey-text"></i>
            <label data-error="wrong" data-success="right" for="orangeForm-pass">Phone Number</label>
            <input type="number" id="orangeForm-pass" class="form-control validate">
            
          </div>
    
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button class="btn submit btn-deep-orange">Submit</button>
        </div>
      </div>
    </div>
    </div>
@include('partials.slideRight')

@include('partials.othernav')

@section("content")
   

      <div class="rentLoan_content container">
         <h5>Loans & Mortgages > Short Term Loan</h5>
          <div class="row">
              <div class="col-md-8">
                  <h2>This product allows customers access loan to support a temporary <br>personal or business capital need. It involves a borrowed capital amount<br> and interest that needs to be returned or paid back <br>at a given full due date, which is usually within a year<br> from getting the loan. <br>Please NB: There is a repayment plan attached <br>to this product..</h2>
                  
                   <p>What you get?</p>
                  <li>Tenor up to 12 months </li>
                  <li>Minimum of N500,000.</li>
                  <li>Customer can access up to N20million</li>
                  <li>Credit Insurance of 1.5%</li>
                  <li>•	Security is Guarantee from an HNI. If loan is above N10million, it will be supported with Equitable<br> Mortgage on a Property with a consent letter to sell in the event of default without recourse to the buyer.</li>
                  
                  <p>Requiremets</p>
                  <li>No equity contribution required</li>
                  <br>
                  
                  <button data-toggle="modal" data-target="#modalRegisterForm">Apply Now <span></span><span></span><span></span><span></span></button>
                  <p class="not"><a href="loanAndMortages.html">Not for you? Check other products</a></p>
                  
                  
                <p>FAQs</p>  
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                     <h4 class="panel-title">
                      How do I know when my application has been approved?
                        </h4>
                    </a>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">We understand how possible it can be to forget when repayment. We have officers and an automation system that would remind you about repayment times.</div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><h4 class="panel-title">
                      Can I pay the loans at once?
                        </h4></a>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse">
                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
                     What happens if I don't meet the deadline for payment?
                        </h4></a>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">We understand how possible it can be to forget when repayment. We have officers and an automation system that would remind you about repayment times.</div>
                  </div>
                </div>
              </div>
              </div>
              <div class="col-md-3">
                  <p>Loans</p>
                  <li><a href="rentLoan.html"><span>>></span> Rent Loan</a></li>
                  <li><a href="shortTermLoan.html"><span>>></span> Short term loan</a></li>
                  
                  <p>Mortages</p>
                  <li><a href="outrightPurchaseMortgage.html"><span>>></span> Outright Purchase Mortgage</a></li>
                  <li><a href="valueAddedLoan.html"><span>>></span> Val Mortgage loan</a></li>
                  <li><a href="constructionFinace.html"><span>>></span> Construction Finance</a></li>
                  <li><a href="contstructionMortgage.html"><span>>></span> Construction Mortgage</a></li>
                  <li><a href="equityRealease.html"><span>>></span> Equity Release</a></li>
                  <li><a href="mortgageRefinance.html"><span>>></span> Mortgage Refinance</a></li>
                  <li><a href="homeImprovement.html"><span>>></span> Home Improvement</a></li>
                  <li><a href="disporaMortgage.html"><span>>></span> Diaspora Mortgage</a></li>
                  <li><a href="generationalMortgage.html"><span>>></span> Generational Mortgage</a></li>
                  <li><a href="microMortgage.html"><span>>></span> Micro Mortgage</a></li>
                  <li><a href="homeOwnershipMortgageAccount.html"><span>>></span>Home Ownership Mortgage Account</a></li>
                  <li><a href="landAcquisition.html"><span>>></span> Land Acquisition</a></li>
                  
                  <div class="number">
                      <p>Need assistance?</p>
                      <li>call <span class="call">+(234) 812 743 3340 </span></li>
                      <li>or <span class="call">send us a mail</span></li>
                      <li>or <span class="call">chat us online</span></li>
                  </div>
                 
              </div>
          </div>
      </div>
      
      <div class="mortgage-content container">
          <div class="top">
            <h5>Similar Products</h5>
            </div>
    <div class="row">
        <div class="col-md-3">
            <div class="inside">
                <div class="image_2"></div>
                <div class="text">
                   <div class="text_inside">
                        <h3>Short Term Loan</h3>
                        <p>Have access to loan to support your temporary personal or business capital need..</p>
                        <button>Apply Now <span></span><span></span><span></span><span></span></button><p class="read"><a href="rentLoan.html">Read More</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
       

<div class="helpful-tool container">
    <h2>Helpful Tools</h2>
    <div class="row">
        <div class="first col-md-4">
            <div class="inn">
                <h3>Mortgage Calculator</h3>
                <p>Find out how much you could borrow or<br> gain in investments</p>
                <p class="footer" data-toggle="modal" data-target="#myModaCalculator"><i class="material-icons">keyboard_arrow_right</i>Check our calculator</p>
            </div>
        </div>
         <div class="col-md-4">
            <div class="inn">
                <h3>Application Form</h3>
                <p>Get started on getting a loan or making an<br> investment with us</p>
                <a href="applicationForm.html"><p class="footer"><i class="material-icons">keyboard_arrow_right</i>Fill our online form</p></a>
            </div>
        </div>
         <div class="col-md-4">
            <div class="inn">
                <h3>FAQs</h3>
                <p>Check out the frequently asked questions from our customers.</p>
                <a href="contact.html"> <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Go to FAQs</p></a>
            </div>
        </div>
    </div>
</div>
    
@endsection