<div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 logo">
                    <img src="{{ asset('images/FirstTrust%20Icon.svg') }}" alt="" class="icon">
                    <a href="/" class="main"><img src="{{ asset('images/Logo%20Grid.svg') }}" alt="" ></a>
                    <i class="fas fa-bars"></i>
                  </div>
                <div class="col-md-9">
                   <div class="top">
                          <p>Social Links : </p>
                          
                          <a href="https://web.facebook.com/ft.mortgagebankplc.7 " target="_blank"><i class="fab fa-facebook-f"></i></a>

                          <a href="https://www.instagram.com/ftmortgagebankplc/" target="_blank"><i class="fab fa-instagram"></i></a>
                           <i class="fab fa-twitter"></i>
                            <i class="fab fa-linkedin-in"></i>
                            <input type="text">
                            <i class="fas fa-search"></i>
                            <i class="material-icons lock">lock</i>
                            <p class="int"><a href="https://ibank.trustbondmortgagebankplc.com/IntBanking" target="_blank"> Internet Banking</a></p>
                   </div>
                    <ul>
                        <li id="acct"><a href="applicationForm">Account</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                        <li class="next_one" id="loan"><a href="#">Loans & Mortgages</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                        <li class="next_one" id="saving"><a href="#">Savings & Investments</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                        <li class="next_one" id="about"><a href="#">About Us</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                        <li class="next_one" id="media"><a href="#">Media</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                        <li class="next_one" id="investor"><a href="#">Investor Relations</a><img src="{{ asset('images/ElegantIcons%20arrow%20carrot%20down.svg') }}" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
 
     <div class="account drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Account</p>
                    <img src="{{ asset('images/Accounts-II@2x.png') }}" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Individual</p>
                    <li><a href="applicationForm">Application form</a></li>
                    <li><a href="loanApplication">Loan Application</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Coorperate</p>
                    <li><a href="applicationForm">Application form</a></li>
                    <li><a href="loanApplication">Loan Application</a></li>
                    
                </div>
                <div class="col-md-3">
                   <i class="material-icons take_away">
                    cancel
                    </i>
                    <p>Agency</p>
                    <li><a href="applicationForm">Application form</a></li>
                    <li><a href="loanApplication">Loan Application</a></li>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="Loan drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Loans & Mortgages</p>
                    <img src="{{ asset('images/Loans-and-mortgages-II@2x.png') }}" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Loan Product</p>
                    <li><a href="shortTermLoan">Short Term Loan</a></li>
                    <li><a href="#">Rent Loan</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Mortgage Product</p>
                    <li><a href="#">Outright Purchase Mortgage</a></li>
                    <li><a href="#">VAL Mortgage Loans</a></li>
                    <li><a href="#">Construction Finance</a></li>
                    <li><a href="#">Construction Mortgage</a></li>
                    <li><a href="#">Equity Release</a></li>
                    <li><a href="#">Mortgage Refinance</a></li>
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                    <br><br>
                    <li><a href="#">Home Improvement</a></li>
                    <li><a href="#">Diaspora Mortgage</a></li>
                    <li><a href="#">Generational Mortgage</a></li>
                    <li><a href="#">Micro Mortgage</a></li>
                    <li><a href="#">Land Acquisition</a></li>
                    <li><a href="#">Home Ownership Mortgage Account</a></li>
                </div>
            </div>
        </div>
    </div>
    <div class="savings drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Savings & Investments</p>
                    <img src="{{ asset('images/Savings@2x.png') }}" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Savings</p>
                    <li><a href="#">Home Plan</a></li>
                    <li><a href="#">Children Savings Account</a></li>
                    <li><a href="#">Target Savings Account</a></li>
                    <li><a href="#">Endowment Savings Account</a></li>
                    <li><a href="#">FirstTrust Premium Account</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Investments</p>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
      <!-- <div class="about-us drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>About Us</p>
                    <img src="images/Savings@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Our Firm</p>
                    <li>Bank Brief</li>
                    <li>The Merger</li>
                    <li>Core Values</li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>The Team</p>
                    <li>Board of Directors</li>
                    <li>Management Team</li>
                    
                    
                </div>
                <div class="col-md-3">
                   <i class="material-icons take_away">
                    cancel
                    </i>
                    <p>Others</p>
                    <li>Code of Governance</li>
                    <li>Careers</li>
                </div>
            </div>
        </div>
    </div> -->
    
    <div class="media drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Media</p>
                    <img src="{{ asset('images/Media@2x.png') }}" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Blog</p>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Videos</a></li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>Resources</p>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">Documents</a></li>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
    <div class="investor drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Investor's Relations</p>
                    <img src="{{ asset('images/Investors%20Relations@2x.png') }}" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Others</p>
                    <li><a href="#">Investors updates & records</a></li>
                    <li><a href="#">Financials</a></li>
                </div>
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
 </div>
    

</div>