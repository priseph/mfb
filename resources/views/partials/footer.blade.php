<div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-6 display-mobile">
                    <img src="{{ asset('images/Logo%20Grid.svg') }}" alt="">
                    <li class="contact_1">+(234) 812 743 3340  </li>
                </div>
                <div class="col-md-4 col-12">
                    <p>Subscribe to our mailing list</p>
                    <p style="font-weight:400;">Get the latest information on product offerings, current trends and much more</p>
                    <input type="text" placeholder="Email Address"><button>Subscribe</button>
                </div>
                <div class="col-md-2 col-6">
                    <p>Quicklinks</p>
                    <li>Application Form   </li>
                    <li>Loan Calculator</li>
                    <li>Interest Rate  Chart</li>
                    <li>Feedback Survey</li>
                    <li>Product Offerings</li>
                </div>
                <div class="col-md-2 col-6">
                    <p>About FirstTrust</p>
                    <li>Our Company  </li>
                    <li>Careers</li>
                    <li>CSR</li>
                    <li>Investor Relations</li>
                    <li><a href="contact.html">Contact Us</a></li>
                </div>
                <div class="col-md-4 col-6 mobile-none">
                    <p>Subscribe to our mailing list</p>
                    <p style="font-weight:400;">Get the latest information on product offerings, current trends and much more</p>
                    <input type="text" placeholder="Email Address"><button>Subscribe</button>
                </div>
                <div class="col-md-1">
                    <li><img src="{{ asset('images/ElegantIcons%20social%20facebook.svg') }}" alt=""></li>
                    <li><img src="{{ asset('images/JamIcons%20instagram.svg') }}" alt=""></li>
                    <li><img src="{{ asset('images/ElegantIcons%20social%20twitter.svg') }}" alt=""></li>
                    <li><img src="{{ asset('images/entypo%20linkedin.svg') }}" alt=""></li>
                    
                </div>
            </div>
        </div>
</div>
    <div class="post-footer">
        <div class="container">
            <p>© 2020 FirstTrust Bank Plc. RC 147538. All Rights Reserved.</p>
        </div>
    </div>