<div class="modal_content animated slideInRight">
    <div class="head">      
      <a href="https://ibank.trustbondmortgagebankplc.com/IntBanking" target="_blank"><i class="material-icons lock">lock</i>Internet Banking</a>
       <i class="fas fa-times"></i>
    </div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                <h4 class="panel-title">
                    Account
                    <i class="fas fa-angle-down"></i>
                </h4>
            </a>
            
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                    <p><a href="applicationForm.html">Accounts</a></p>
                    <li><a href="applicationForm.html">Application Form</a></li>
                    <li><a href="loanApplication.html"> Loan Application</a></li>
                
            </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><h4 class="panel-title">
                Loan & Mortages<i class="fas fa-angle-down"></i>
                </h4></a>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                    <p><a href="loanAndMortages.html">Loan & Mortages</a></p>
                    <li><a href="rentLoan.html">Rent Loan</a></li>
                    <li><a href="shortTermLoan.html">Short Term Loan</a></li>

                    <p>Mortage Product</p>
                    <li><a href="outrightPurchaseMortgage.html">Outright Purchase Mortgage</a></li>
                    <li><a href="valueAddedLoan.html">VAL Mortgage Loans</a></li>
                    <li><a href="constructionFinace.html">Construction Finance</a></li>
                    <li><a href="contstructionMortgage.html">Construction Mortgage</a></li>
                    <li><a href="equityRealease.html">Equity Release</a></li>
                    <li><a href="mortgageRefinance.html">Mortgage Refinance</a></li>
                    <li><a href="homeImprovement.html">Home Improvement</a></li>
                    <li><a href="disporaMortgage.html">Diaspora Mortgage</a></li>
                    <li><a href="generationalMortgage.html">Generational Mortgage</a></li>
                    <li><a href="microMortgage.html">Micro Mortgage</a></li>
                    <li><a href="landAcquisition.html">Land Acquisition</a></li>
                    <li><a href="homeOwnershipMortgageAccount.html">Home Ownership Mortgage Account</a></li>
                
            </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
                    Savings & Investments <i class="fas fa-angle-down"></i>
                </h4></a>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
                    <p><a href="savingAndInvestment.html">Savings And Investments</a></p>
                    <li><a href="homePlan.html">Home Plan</a></li>
                    <li><a href="childrenSavingsAccount.html">Children Savings Account</a></li>
                    <li><a href="targetSavingAccount.html">Target Savings Account</a></li>
                    <li><a href="endowmentSavingsAccount.html">Endowment Savings Account</a></li>
                    <li><a href="firsttrustPremiumAccount.html">FirstTrust Premium Account</a></li>
                
            </div>
            </div>
        </div>

        <div class="panel panel-default">
                <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><h4 class="panel-title">
                        About Us <i class="fas fa-angle-down"></i>
                    </h4></a>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <li><a href="aboutUs.html">About Us</a></li>
                </div>
                </div>
            </div>

                <div class="panel panel-default">
                        <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><h4 class="panel-title">
                                Media<i class="fas fa-angle-down"></i>
                            </h4></a>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                                <li><a href="media.html">Media</a></li>
                        </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><h4 class="panel-title">
                                Investors Relations
                            </h4></a>
                        </div>

                        <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <li><a href="investorUpdate.html">Investors updates & records</a></li>
                            <li><a href="financial.html">Financials</a></li>
                        </div>
                        </div>
                    </div>

        </div>

        <h2>Quick Links</h2>
        <div class="quick">
            <li><a href="applicationForm.html">Open an Account</a></li>
            <li><a href="https://ibank.trustbondmortgagebankplc.com/IntBanking" target="_blank">Internet Banking</a></li>
            <li><a href="#"  data-toggle="modal" data-target="#myModaCalculator">Loan Calculator</a></li>
            <li><a href="savingAndInvestment.html">Investmnt Plan</a></li>
            <li><a href="contact.html">Customer Service</a></li>
        </div>

</div>