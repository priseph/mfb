@extends("layouts.masters")
  
@include('partials.navbar')
  
@include('partials.slideRight')
@section("content")

    <div class="Loan drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Loans & Mortgages</p>
                    <img src="images/Loans-and-mortgages-II@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Loan Product</p>
                    <li><a href="shortTermLoan.html">Short Term Loan</a></li>
                    <li><a href="rentLoan.html">Rent Loan</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Mortgage Product</p>
                    <li><a href="outrightPurchaseMortgage.html">Outright Purchase Mortgage</a></li>
                    <li><a href="valueAddedLoan.html">VAL Mortgage Loans</a></li>
                    <li><a href="constructionFinace.html">Construction Finance</a></li>
                    <li><a href="contstructionMortgage.html">Construction Mortgage</a></li>
                    <li><a href="equityRealease.html">Equity Release</a></li>
                    <li><a href="mortgageRefinance.html">Mortgage Refinance</a></li>
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                    <br><br>
                    <li><a href="homeImprovement.html">Home Improvement</a></li>
                    <li><a href="disporaMortgage.html">Diaspora Mortgage</a></li>
                    <li><a href="generationalMortgage.html">Generational Mortgage</a></li>
                    <li><a href="microMortgage.html">Micro Mortgage</a></li>
                    <li><a href="landAcquisition.html">Land Acquisition</a></li>
                    <li><a href="homeOwnershipMortgageAccount.html">Home Ownership Mortgage Account</a></li>
                </div>
            </div>
        </div>
    </div>
    <div class="savings drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Savings & Investments</p>
                    <img src="images/Savings@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Savings</p>
                    <li><a href="homePlan.html">Home Plan</a></li>
                    <li><a href="childrenSavingsAccount.html">Children Savings Account</a></li>
                    <li><a href="targetSavingAccount.html">Target Savings Account</a></li>
                    <li><a href="endowmentSavingsAccount.html">Endowment Savings Account</a></li>
                    <li><a href="firsttrustPremiumAccount.html">FirstTrust Premium Account</a></li>
                    
                </div>
                <div class="col-md-3">
                    <p>Investments</p>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
      <!-- <div class="about-us drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>About Us</p>
                    <img src="images/Savings@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Our Firm</p>
                    <li>Bank Brief</li>
                    <li>The Merger</li>
                    <li>Core Values</li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>The Team</p>
                    <li>Board of Directors</li>
                    <li>Management Team</li>
                    
                    
                </div>
                <div class="col-md-3">
                   <i class="material-icons take_away">
                    cancel
                    </i>
                    <p>Others</p>
                    <li>Code of Governance</li>
                    <li>Careers</li>
                </div>
            </div>
        </div>
    </div> -->
    
    <div class="media drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Media</p>
                    <img src="images/Media@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Blog</p>
                    <li><a href="new.html">News</a></li>
                    <li><a href="video.html">Videos</a></li>
                    
                    
                </div>
                <div class="col-md-3">
                    <p>Resources</p>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="document.html">Documents</a></li>
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
    <div class="investor drop">
        <div class="container">
            <div class="row">
                <div class="first col-md-3">
                   <p>Investor's Relations</p>
                    <img src="images/Investors%20Relations@2x.png" alt="">
                    
                </div>
                <div class="col-md-3">
                    <p>Others</p>
                    <li><a href="investorUpdate.html">Investors updates & records</a></li>
                    <li><a href="financial.html">Financials</a></li>
                </div>
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-3">
                    <i class="material-icons take_away">
                    cancel
                    </i>
                </div>
            </div>
        </div>
    </div>
 </div>
    
    <div class="container cooperate-application">
        <div class="row">
            <div class="col-md-4 list_one" >
                <a href="applicationForm.html">CORPORATE APPLICATION</a>
            </div>
            <div class="col-md-4 list_two" >
                <a href="individualApplication.html"  >INDIVIDUAL APPLICATION</a>
            </div>
            <div class="col-md-4 list_three" style="color:#f7f7f7; background-color:#EAAB26;">
                <a href="loanApplication.html" style="color:#f7f7f7">LOAN APPLICATION</a>
            </div>
        </div>  
    </div><br>
      
      <div class="container thetoggleTabs">
      <div class="toppy">
       <p>Mortgage Loan Form</p>
        <ul class= "nav nav-tabs" role="tablist">
           
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#home">Particulars Of Borrower</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu1">Personal Financial Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu2">Particulars Of The Loan</a>
            </li>
          </ul>
        </div>
              <!-- Tab panes -->
              
              <div class="tab-content">
                <div id="home" class="container tab-pane active" style="border:none;"><br>
                <div class="row">
                  <div class="col-md-3 imaggge">
                     <div class="avatar img-uploader" id="img-uploader"></div>
                     
                      <div class="">
                        <form class="" enctype="multipart/form-data" method="post">
                          <input type="file" id="file-upload" style="height: 1px;">
                        </form>
                      </div>

                  </div>
                  <div class="col-md-9">
                      <div class="row" style="margin:0;">
                        <div id="p" class="form-group col-md-4" style="padding-left:0;">
                          <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">First Name</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>

                        <div id="p" class="form-group col-md-4" style="padding:0px;">
                          <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">Surname</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                        
                        <div id="p" class="form-group col-md-4" style="padding:0px;">
                          <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">Middle Name</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                    </div>
                    

                <div id="p" class="form-group" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">First Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group" style="padding:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Middle Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                 
                 <div class="row" style="margin:0;">
                        <div id="p" class="form-group col-md-6" style="padding-left:0;">
                          <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">Nickname</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>

                        <div id="p" class="form-group col-md-6" style="padding:0px;">
                           <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                          <option value=""></option>
                          <option value="Female">Female</option>
                          <option value="Male">Male</option>
                          </select>
                          <label for="text" class="float-label">Gender</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                    </div>
                    
                    <div class="row" style="margin:0;">
                        <div id="p" class="form-group col-md-6" style="padding-left:0;">
                           <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                              <option value=""></option>
                              <option value="Female">Married</option>
                              <option value="Male">Single</option>
                              <option value="Male">Divorced</option>
                          </select>
                          <label for="text" class="float-label">Marital Status</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>

                        <div id="p" class="form-group col-md-6" style="padding:0px;">
                          <input id="Date" class="form-control" spellcheck=false name="date" type="Date" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="Date" class="float-label">Date of Birth</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                    </div>
                  <div id="p" class="form-group">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="password" class="float-label">Mother's Maiden Name</label>
                  <errorp>
                     it is required
                  </errorp>
                </div>
                
                 <div class="row" style="margin:0;">
                        <div id="p" class="form-group col-md-6" style="padding-left:0;">
                            <textarea id="text" class="form-control" spellcheck=false name="text" type="text" alt="login" required=""></textarea>
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">Nationality (For Non-Nigerians)</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>

                        <div id="p" class="form-group col-md-6" style="padding:0px;">
                            <textarea id="text" class="form-control" spellcheck=false name="text" type="text" alt="login" required=""></textarea>
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="Date" class="float-label">Residential Permit No.</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                    </div>
                    
                     <div class="row" style="margin:0;">
                        <div id="p" class="form-group col-md-6" style="padding-left:0;">
                          <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">State of Origin</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>

                        <div id="p" class="form-group col-md-6" style="padding:0px;">
                          <input id="text" class="form-control" spellcheck=false name="text" type="text" alt="login" required="">
                          <span class="form-highlight"></span>
                          <span class="form-bar"></span>
                          <label for="text" class="float-label">LGA</label>
                          <errorp>
                            it is required
                          </errorp>
                        </div>
                    </div>
                
                  </div>
                </div>
                    <a data-toggle="tab" href="#menu1"><button>Next</button></a>
            </div>
                
                
                

            <div id="menu1" class="container tab-pane fade"><br>
              <form action="">
            <h1>Residential Address</h1>
              <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="name" class="form-control" spellcheck=false name="nmae" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            
            
            <div id="p" class="form-group" style="padding:0px;">
              <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">City</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Nearest Busstop</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Phone Number(1)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding:0px;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Phone Number(2)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>

              <div id="p" class="form-group">
              <input id="email" class="form-control" spellcheck=false name="email" type="email" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="Email" class="float-label">Email</label>
              <errorp>
                it is required
              </errorp>
            </div>
            
            <h1>Means of Identification</h1>
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                   <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                      <option value=""></option>
                      <option value="Female">National Passport</option>
                      <option value="Male">Driver's License</option>
                      <option value="Male">Voter's Card</option>
                  </select>
                  <label for="text" class="float-label">ID Card Type</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding:0px;">
                  <input id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Other(Specify)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
             
             <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-4" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">ID No</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="date" class="form-control" spellcheck=false name="date" type="date" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="date" class="float-label">ID Issue Date</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
                
                <div id="p" class="form-group col-md-4" style="padding:0px;">
                  <input id="date" class="form-control" spellcheck=false name="date" type="date" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="date" class="float-label">ID Expiry Date</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            </form>
            <a data-toggle="tab" href="#home"><button>Previous</button></a>
            <a data-toggle="tab" href="#menu2"><button>Next</button></a>
            </div>
            
            
            <div id="menu2" class="container tab-pane fade"><br>
              <form action="">
              <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <select id="number" class="form-control" spellcheck=false name="gender" type="select" alt="login" required="">
                      <option value=""></option>
                      <option value="Female">Employed</option>
                      <option value="Male">Unemployed</option>
                      <option value="Male">Self Employed</option>
                  </select>
                  <label for="text" class="float-label">Employment Status</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Other(Specify)</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
             
             <div id="p" class="form-group" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Employer's Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
              
            <h1>Employer's/Employment Address (Even if self employed.)</h1>
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="name" class="form-control" spellcheck=false name="nmae" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            <div id="p" class="form-group" style="padding:0px;">
              <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">City</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Nearest Busstop</label>
              <errorp>
                it is required
              </errorp>
            </div>
            
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Nature of Business</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Office Phone Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding:0px;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Fax  Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            
              <h1>Details of Next of Kin.</h1>
              <div id="p" class="form-group">
              <input id="name" class="form-control" spellcheck=false name="name" type="text" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="Email" class="float-label">Surname</label>
              <errorp>
                it is required
              </errorp>
            </div>
            
            <div id="p" class="form-group">
              <input id="name" class="form-control" spellcheck=false name="name" type="text" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="Email" class="float-label">Middle Name</label>
              <errorp>
                it is required
              </errorp>
            </div>
            
            <div id="p" class="form-group">
              <input id="name" class="form-control" spellcheck=false name="name" type="text" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="Email" class="float-label">Last Name</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="name" class="form-control" spellcheck=false name="nmae" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding-left:0px;">
                  <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Street Name</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
            <div id="p" class="form-group" style="padding:0px;">
              <input id="name" class="form-control" spellcheck=false name="name" type="name" size="18" alt="login" required="">
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">City</label>
              <errorp>
                it is required
              </errorp>
            </div>
            <div id="p" class="form-group">
              <textarea id="text" class="form-control" spellcheck=false name="text" type="text" size="18" alt="login" required=""></textarea>
              <span class="form-highlight"></span>
              <span class="form-bar"></span>
              <label for="text" class="float-label">Nearest Busstop</label>
              <errorp>
                it is required
              </errorp>
            </div>
            
            <div class="row" style="margin:0;">
                <div id="p" class="form-group col-md-6" style="padding-left:0;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Mobile Number</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>

                <div id="p" class="form-group col-md-6" style="padding:0px;">
                  <input id="number" class="form-control" spellcheck=false name="number" type="number" size="18" alt="login" required="">
                  <span class="form-highlight"></span>
                  <span class="form-bar"></span>
                  <label for="text" class="float-label">Relationship</label>
                  <errorp>
                    it is required
                  </errorp>
                </div>
            </div>
          
             
            </form>
            <a data-toggle="tab" href="#home"><button>Previous</button></a>
            <button>Submit</button>
            </div>
          </div>
        </div>   

<!--
    <div class="helpful-tool container">
        <h2>Helpful Tools</h2>
        <div class="row">
            <div class="first col-md-4">
                <div class="inn">
                    <h3>Mortgage Calculator</h3>
                    <p>Find out how much you could borrow or<br> gain in investments</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Check our calculator</p>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>Application Form</h3>
                    <p>Get started on getting a loan or making an<br> investment with us</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Fill our online form</p>
                </div>
            </div>
             <div class="col-md-4">
                <div class="inn">
                    <h3>Fill our online form</h3>
                    <p>Check out the frequently asked questions from our customers.</p>
                    <p class="footer"><i class="material-icons">keyboard_arrow_right</i>Go to FAQs</p>
                </div>
            </div>
        </div>
    </div>
-->
    
 @endsection